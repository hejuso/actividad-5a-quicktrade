package com.example.hejuso.firebaseprojecte.Controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.example.hejuso.firebaseprojecte.Model.Producto;
import com.example.hejuso.firebaseprojecte.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MostrarProductosActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    DatabaseReference bbdd;
    ListView lista;
    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar_productos);

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();

        Button botonAnyadir = findViewById(R.id.botonAnyadir);
        lista = findViewById(R.id.lista_productos);
        Button mis_productos = findViewById(R.id.mis_productos);

        bbdd = FirebaseDatabase.getInstance().getReference(getString(R.string.nodo_productos));

        bbdd.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                ArrayAdapter<String> adaptador;
                ArrayList<String> listado = new ArrayList<>();

                // Recorremos el array de los datos de firebase
                for(DataSnapshot datasnapshot: dataSnapshot.getChildren()){

                    Producto prod = datasnapshot.getValue(Producto.class);
                    String uid_usuario = user.getUid().toString();
                    String uid_producto = prod.getUid().toString();

                    if (!uid_usuario.equals(uid_producto)) {
                        String nombre_prod = prod.getNombre();
                        listado.add(nombre_prod);
                    }else{
                        String nombre_prod = prod.getNombre();
                        listado.add(nombre_prod+" (modificable)");
                    }

                }

                adaptador = new ArrayAdapter<>(MostrarProductosActivity.this, android.R.layout.simple_list_item_1,listado);
                lista.setAdapter(adaptador);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        botonAnyadir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent listaIntent = new Intent(getApplicationContext(),
                        AnyadirProductoActivity.class);

                startActivity(listaIntent);
            }
        });


        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                Intent listaIntent = new Intent(getApplicationContext(),
                        ModificarProductoActivity.class);
                listaIntent.putExtra("nombre", parent.getItemAtPosition(position).toString());
                startActivity(listaIntent);
            }
        });


        mis_productos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                bbdd.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        ArrayAdapter<String> adaptador;
                        ArrayList<String> listado = new ArrayList<>();

                        // Recorremos el array de los datos de firebase
                        for(DataSnapshot datasnapshot: dataSnapshot.getChildren()){

                            Producto prod = datasnapshot.getValue(Producto.class);
                            String uid_usuario = user.getUid().toString();
                            String uid_producto = prod.getUid().toString();

                            if (uid_usuario.equals(uid_producto)) {
                                String nombre_prod = prod.getNombre();
                                listado.add(nombre_prod+" (modificable)");
                            }

                        }

                        adaptador = new ArrayAdapter<>(MostrarProductosActivity.this, android.R.layout.simple_list_item_1,listado);
                        lista.setAdapter(adaptador);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }
        });


    }
}
