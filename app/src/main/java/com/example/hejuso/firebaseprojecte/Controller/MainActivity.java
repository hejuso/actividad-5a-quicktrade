package com.example.hejuso.firebaseprojecte.Controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.hejuso.firebaseprojecte.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button registro = findViewById(R.id.botonRegistro);
        Button inicio_sesion = findViewById(R.id.botonIniciarSesion);

        registro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent mainIntent = new Intent(getApplicationContext(),
                        RegistreActivity.class);
                startActivity(mainIntent);
            }
        });


        inicio_sesion.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent login = new Intent(getApplicationContext(),
                        InicioSesionActivity.class);
                startActivity(login);
            }
        });

    }

}
