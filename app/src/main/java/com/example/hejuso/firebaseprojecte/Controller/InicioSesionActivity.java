package com.example.hejuso.firebaseprojecte.Controller;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.hejuso.firebaseprojecte.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


public class InicioSesionActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseUser user;
    EditText text_email;
    EditText text_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio_sesion);

        text_email = findViewById(R.id.email_login);
        text_password = findViewById(R.id.password_login);

        Button inicio_sesion = findViewById(R.id.botonIniciarSesion);

        inicio_sesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String email = text_email.getText().toString();
                String password = text_password.getText().toString();
                login(email,password);
            }
        });
    }

    private void login(String email, String password){

        mAuth = FirebaseAuth.getInstance();

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            user = mAuth.getCurrentUser();

                            Intent productos_mostrar = new Intent(getApplicationContext(),
                                    MostrarProductosActivity.class);
                            startActivity(productos_mostrar);

                        }else{
                            Toast.makeText(InicioSesionActivity.this, "Falló la autentificación.", Toast.LENGTH_SHORT).show();
                        }

                    }
                });

    }

}
