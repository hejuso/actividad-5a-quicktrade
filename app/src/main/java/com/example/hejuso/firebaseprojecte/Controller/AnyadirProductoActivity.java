package com.example.hejuso.firebaseprojecte.Controller;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.hejuso.firebaseprojecte.Model.Producto;
import com.example.hejuso.firebaseprojecte.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AnyadirProductoActivity extends AppCompatActivity {
    DatabaseReference bbdd;
    EditText text_descripcion;
    EditText text_nombre;
    EditText text_categoria;
    EditText text_precio;
    private FirebaseAuth mAuth;
    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anyadir_producto);

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();

        text_nombre = findViewById(R.id.nombre);
        text_descripcion = findViewById(R.id.descripcion);
        text_categoria = findViewById(R.id.categoria);
        text_precio = findViewById(R.id.precio);


        Button anyadir_producto = findViewById(R.id.anyadir_producto);
        bbdd = FirebaseDatabase.getInstance().getReference(getString(R.string.nodo_productos));

        anyadir_producto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Producto d = new Producto(text_nombre.getText().toString(), text_descripcion.getText().toString(), text_categoria.getText().toString(), text_precio.getText().toString(),user.getUid());

                String clave = bbdd.push().getKey();

                bbdd.child(clave).setValue(d);

                Toast.makeText(AnyadirProductoActivity.this, "Producto añadido", Toast.LENGTH_LONG).show();
            }
        });


    }
}
