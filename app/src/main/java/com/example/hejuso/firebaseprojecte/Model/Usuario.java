package com.example.hejuso.firebaseprojecte.Model;

/**
 * Created by hejuso on 09/02/2018.
 */

public class Usuario {

    private String email;
    private String nombre;
    private String apellidos;
    private String direccion;
    private String uid;


    public Usuario(){

    }

    public Usuario(String email, String nombre, String apellidos, String direccion, String uid) {
        this.email = email;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.uid = uid;

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getUid() {
        return uid;
    }
    public void setUid(String uid) {
        this.uid = uid;
    }
    @Override
    public String toString() {
        return "Usuario{" +
                "email='" + email + '\'' +
                ", uid='" + uid + '\'' +
                ", nombre='" + nombre + '\'' +
                ", apellidos='" + apellidos + '\'' +
                ", direccion='" + direccion + '\'' +
                '}';
    }
}
