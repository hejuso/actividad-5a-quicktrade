package com.example.hejuso.firebaseprojecte.Controller;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.example.hejuso.firebaseprojecte.Model.Usuario;
import com.example.hejuso.firebaseprojecte.R;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class RegistreActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    DatabaseReference bbdd;
    FirebaseUser user;
    EditText text_email;
    EditText text_password;
    EditText text_nombre;
    EditText text_apellidos;

    EditText text_direccion;

    boolean existe = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registre);

        text_email = findViewById(R.id.email);
        text_password = findViewById(R.id.contrasenya);

        text_nombre = findViewById(R.id.nombre);
        text_apellidos = findViewById(R.id.apellidos);
        text_direccion = findViewById(R.id.direccion);

        Button anyadir_usuario = findViewById(R.id.anyadir_usuario);

        bbdd = FirebaseDatabase.getInstance().getReference(getString(R.string.nodo_usuarios));

        anyadir_usuario.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                String email = text_email.getText().toString();
                String nombre = text_nombre.getText().toString();
                String apellidos = text_apellidos.getText().toString();
                String direccion = text_direccion.getText().toString();
                String password = text_password.getText().toString();

                if (TextUtils.isEmpty(email) || TextUtils.isEmpty(nombre) || TextUtils.isEmpty(apellidos) || TextUtils.isEmpty(direccion)){
                    Toast.makeText(RegistreActivity.this, "Introduce todos los datos", Toast.LENGTH_SHORT).show();

                }else{

                    registrar(email,password,apellidos,direccion);

                }

            }
        });

    }



    private void registrar(String email, String password, String apellidos, String direccion){

        mAuth = FirebaseAuth.getInstance();

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            user = mAuth.getCurrentUser();

                            Query q = bbdd.orderByChild(getString(R.string.nodo_email)).equalTo(user.getEmail());

                            q.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {

                                    for(DataSnapshot datasnapshot: dataSnapshot.getChildren()){
                                        Usuario usuario = datasnapshot.getValue(Usuario.class);
                                        String email_usuario = usuario.getEmail();

                                        if (email_usuario.equals(text_email.getText().toString())) {
                                            existe = true;
                                        }else{
                                            existe = false;
                                        }
                                    }

                                    if (!existe){

                                        Usuario u = new Usuario(text_email.getText().toString(),text_nombre.getText().toString(),text_apellidos.getText().toString(),text_direccion.getText().toString(),user.getUid());

                                        String clave = bbdd.push().getKey();

                                        bbdd.child(clave).setValue(u);

                                        Toast.makeText(RegistreActivity.this, "Inicio de sesión satisfactorio. "+user.getUid(), Toast.LENGTH_SHORT).show();

                                        FirebaseAuth.getInstance().signOut();
                                        Intent mainIntent = new Intent(getApplicationContext(),
                                                InicioSesionActivity.class);
                                        startActivity(mainIntent);

                                    }else{
                                        Toast.makeText(RegistreActivity.this, "Ese usuario ya existe", Toast.LENGTH_SHORT).show();

                                    }

                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });

                        } else {
                            Toast.makeText(RegistreActivity.this, "Falló la autentificación.", Toast.LENGTH_SHORT).show();
                        }

                    }
                });

    }


}