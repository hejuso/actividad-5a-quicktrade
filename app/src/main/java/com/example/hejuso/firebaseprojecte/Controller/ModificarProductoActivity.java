package com.example.hejuso.firebaseprojecte.Controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.hejuso.firebaseprojecte.Model.Producto;
import com.example.hejuso.firebaseprojecte.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class ModificarProductoActivity extends AppCompatActivity {

    private FirebaseUser user;

    private FirebaseAuth mAuth;
    DatabaseReference bbdd;
    EditText text_descripcion;
    EditText text_nombre;
    EditText text_categoria;
    EditText text_precio;
    String nombre_intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar_producto);
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();

        bbdd = FirebaseDatabase.getInstance().getReference(getString(R.string.nodo_productos));

        // El uid que le pasamos de la anterior activity
        nombre_intent = getIntent().getExtras().getString("nombre");

        text_nombre = findViewById(R.id.nombre);
        text_descripcion = findViewById(R.id.descripcion);
        text_categoria = findViewById(R.id.categoria);
        text_precio = findViewById(R.id.precio);


        // Recogemos el producto que coincida
        Query q = bbdd.orderByChild(getString(R.string.nodo_nombre)).equalTo(nombre_intent);

        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot datasnapshot: dataSnapshot.getChildren()){
                    Producto prod = datasnapshot.getValue(Producto.class);
                    String uid_usuario = user.getUid().toString();
                    String uid_producto = prod.getUid().toString();

                    if (!uid_usuario.equals(uid_producto)){
                        Toast.makeText(ModificarProductoActivity.this, "Este producto no es tuyo.", Toast.LENGTH_SHORT).show();
                        Intent atras = new Intent(getApplicationContext(),
                                MostrarProductosActivity.class);
                        startActivity(atras);
                    }
                    text_descripcion.setText(prod.getDescripcion());
                    text_nombre.setText(prod.getNombre());
                    text_categoria.setText(prod.getCategoria());
                    text_precio.setText(prod.getPrecio());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        Button modificar_producto = findViewById(R.id.modificar_producto);

        modificar_producto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String descripcion = text_descripcion.getText().toString();
                String nombre = text_nombre.getText().toString();
                String categoria = text_categoria.getText().toString();
                String precio = text_precio.getText().toString();

                if (TextUtils.isEmpty(descripcion) || TextUtils.isEmpty(nombre) || TextUtils.isEmpty(categoria) || TextUtils.isEmpty(precio)){
                    Toast.makeText(ModificarProductoActivity.this, "Introduce todos los datos", Toast.LENGTH_SHORT).show();

                }else{

                    Query q = bbdd.orderByChild(getString(R.string.nodo_nombre)).equalTo(nombre_intent);

                    q.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            for(DataSnapshot datasnapshot: dataSnapshot.getChildren()){
                                String clave = datasnapshot.getKey();
                                bbdd.child(clave).child(getString(R.string.nodo_descripcion)).setValue(text_descripcion.getText().toString());
                                bbdd.child(clave).child(getString(R.string.nodo_nombre)).setValue(text_nombre.getText().toString());
                                bbdd.child(clave).child(getString(R.string.nodo_categoria)).setValue(text_categoria.getText().toString());
                                bbdd.child(clave).child(getString(R.string.nodo_precio)).setValue(text_precio.getText().toString());
                            }
                            Toast.makeText(ModificarProductoActivity.this, "El producto ha sido modificado.", Toast.LENGTH_SHORT).show();

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }

            }
        });

    }
}
